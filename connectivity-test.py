import requests
from time import sleep

def test_connection(host,port,proxy):
    proxy_var = bool(proxy)
    try:
        if proxy_var:
            with requests.get('https://{host}:{port}', timeout=5, proxies=proxy, stream=True) as r:
                status_code = r.status_code
                remote_ip = r.raw._original_response.fp.raw._sock.getpeername()[0]
        else:
            with requests.get(f'https://{host}:{port}', timeout=5, stream=True) as r:
                status_code = r.status_code
                remote_ip = r.raw._original_response.fp.raw._sock.getpeername()[0]
        print(f"[+] Request to: {host} on remote ip: {remote_ip} on port {port} result: {status_code}")
        return True
    except Exception as e:
        print(f"[-] FAILED - Unable to resolve - {host}")
        return False


if __name__ == '__main__':
    sites = [
        ('{}.blob.core.windows.net'.format(blob), 443),
        ('{}.table.core.windows.net'.format(blob), 443),
        ('analysis-{}.aidoc-nsps.com'.format(analysis), 443),
        ('cd.aidoc-nsps.com', 443),
        ('consul.aidoc-nsps.com', 443),
        ('band.aidoc-nsps.com', 443),
        ('dc.applicationinsights.microsoft.com', 443),
        ('rt.services.visualstudio.com', 443),
        ('breeze.aimon.applicationinsights.io', 443),
        ('rt.applicationinsights.microsoft.com', 443),
        ('dc.services.visualstudio.com', 443)
   ]

    proxy = "$proxy_input"
    if proxy == "no proxy":
        proxy_servers = {}
    else:
        proxy_servers = {"http": proxy,
                        "https": proxy}

    success_counter = 0
    for _ in range(times):
        for site in sites:
            if test_connection(site[0], site[1], proxy_servers):
                success_counter += 1
        sleep(0.2)
    print(f"\nSuccessful ip resolves: {success_counter}/{times * len(sites)}")
